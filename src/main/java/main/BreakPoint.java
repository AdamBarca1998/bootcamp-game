package main;

import java.util.*;

import data.Transition;

public class BreakPoint {
	public String name;
	private List<Transition> transitions = new ArrayList<Transition>();
	
	public BreakPoint(String name) {
		this.name = name;
	}

	public void addTransition(Transition transition) {
		transitions.add(transition);
	}
	
	/**
	 * Method searches transitions where the symbols match. 
	 * If find this transitions returns new state, or if no find transition with same symbols return null.
	 * @param with concrete symbol
	 * @return newState or null
	 */
	public String move(String with) {
		for (Transition i: transitions) {
			if ( with.equals(i.getWith()) ) {
				return i.getTo();
			}
		}
		
		return null;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append(name + "\t -> ");
		for (Transition i: transitions) {
			sb.append(i.getWith() + "\n");
			sb.append("\t|\n\t -> ");
		}
		sb.delete(sb.length()-7, sb.length());
		sb.append("\n");
		
		return sb.toString();
	}
}
