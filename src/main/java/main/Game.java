package main;

import java.util.List;
import java.util.Scanner;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import converters.FileToJson;
import converters.JsonToBreakPoints;

public class Game {

	int hp;
	JsonArray finalStates = new JsonArray();
	
	public Game(int hp) {
		this.hp = hp;
	}
	
	/**
	 * Method on initialization objects and launch the application.
	 */
	public void start() {
		FileToJson map = new FileToJson();
		JsonObject jsonObject = map.getMap("maps/map1.json");
		
		JsonToBreakPoints jtbp = new JsonToBreakPoints();
		List<BreakPoint> list = jtbp.getBreakPoints(jsonObject);
		
		finalStates = jsonObject.getAsJsonArray("finalStates");
		
		play(list);
	}
	
	private void play(List<BreakPoint> breakPoints) {
		Scanner sc = new Scanner(System.in);
		int index = 0;
		boolean writeBreakPoints = true;
		
		printMsg("START You hp: " + hp);

		
		while(true) {
			BreakPoint bp = breakPoints.get(index);
			
			if (writeBreakPoints) {
				System.out.println(bp);
				
				System.out.print("Do you think you found the theasure? [y/n]");
				String a = sc.next();
				if (a.equals("y")) {
					if (isTreasure(bp.name)) {
						printMsg("YOU WIN!");
						break;
					} else {
						hp -= 1;
						if (hp == 0) {
							printMsg("You DEAD!");
							break;
						} else {
							printMsg("Your hp is: " + hp);
						}
					}
				}
			}
			writeBreakPoints = true;
			
			System.out.print("Enter the way you want to go: ");
			String s = sc.next();
			if (s == "exit") {
				printMsg("EXIT GAME");
				break;
			}
			String with = "\"" + s + "\"";
			
			String newState = bp.move(with);
			
			if (newState != null) {
				index = setNewBreakPoint(bp.move(with), breakPoints);
				
				if (index == -1) {
					printMsg("YOU LOSE!");
					break;
				}
			} else {
				System.out.println("ERROR: this way not existing " + with);
				writeBreakPoints = false;
			}
		}
	}
	
	private boolean isTreasure(String state) {
		for (JsonElement i: finalStates) {
			if ( state.equals(i.toString()) ) {
				return true;
			}
		}
		
		return false;
	}
	
	private int setNewBreakPoint(String state, List<BreakPoint> breakPoints) {	
		for (int i = 0; i < breakPoints.size(); i++) {
			if ( state.equals(breakPoints.get(i).name) ) {
				return i;
			}
		}
		
		return -1;
	}
	
	private void printMsg(String msg) {
		
		StringBuilder lineTop = new StringBuilder();
		StringBuilder lineMiddle = new StringBuilder();
				
		lineTop.append("--------------------------");
		lineMiddle.append("------- " + msg + " -------");
				
		StringBuilder smaller;
		if (lineTop.length() < lineMiddle.length()) {
			smaller = lineTop;
		} else {
			smaller = lineMiddle;
		}
	
		boolean toFirstOrLast = false;
		while (lineTop.length() != lineMiddle.length()) {
			if (!toFirstOrLast) {
				smaller.insert(0, "-");
			} else {
				smaller.append("-");
			}
			toFirstOrLast = !toFirstOrLast;
		}
		
		System.out.println(lineTop);
		System.out.println(lineMiddle);
		System.out.println(lineTop);
	}
}
