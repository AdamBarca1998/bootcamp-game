package data;

public class Transition {
	private String to;
	private String from;
	private String with;
	
	public Transition(String with, String to, String from) {
		this.with = with;
		this.to = to;
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public String getWith() {
		return with;
	}

	public String getFrom() {
		return from;
	}
}
