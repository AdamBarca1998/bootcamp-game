package converters;

import java.io.*;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class FileToJson {
	
	/**
	 * Method on convert file.json on jsonObject.
	 * Method convert only files from resource.
	 * @param fileName name specific file.json
	 * @return converted jsonObject
	 */
	public JsonObject getMap(String fileName) {
		InputStream is = getFileFromResourceAsStream(fileName);
		
	    //https://www.baeldung.com/gson-string-to-jsonobject
	    JsonObject jsonObject = new JsonParser().parse(getStringFromInputStream(is)).getAsJsonObject();	
	    
	    return jsonObject;
	}
	
	/*
	 * https://mkyong.com/java/java-read-a-file-from-resources-folder/
	 */
	private InputStream getFileFromResourceAsStream(String fileName) {
        // The class loader that loaded the class
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream(fileName);

        // the stream holding the file content
        if (inputStream == null) {
            throw new IllegalArgumentException("file not found! " + fileName);
        } else {
            return inputStream;
        }
    }
	
	/*
	 * https://stackoverflow.com/questions/22461663/convert-inputstream-to-jsonobject
	 */
	private String getStringFromInputStream(InputStream is) {
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
	    StringBuilder sb = new StringBuilder();
	    String line;
	    try {
			while ((line = br.readLine()) != null) {
			    sb.append(line+"\n");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    try {
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    return sb.toString();
	}
}
