package converters;

import java.util.*;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import data.Transition;
import main.BreakPoint;

public class JsonToBreakPoints {
	
	/**
	 * Method on covert jsonObject on list BreakPoints.
	 * Method does not check whether the input jsonObject is valid.
	 * @param jsonObject object specific jsonObject 
	 * @return list BreakPoints
	 */
	public List<BreakPoint> getBreakPoints(JsonObject jsonObject) {
		List<BreakPoint> breakPoints = new ArrayList<BreakPoint>();
		
		JsonArray transitionsArr = new JsonArray();
		transitionsArr = jsonObject.getAsJsonArray("transitions");
		
		for (JsonElement i: transitionsArr) {
			JsonObject obj = i.getAsJsonObject();
			
			String name = obj.get("from").toString();
			
			BreakPoint bp = returnPoint(name, breakPoints);
			Transition tran = new Transition(obj.get("with").toString(), obj.get("to").toString(), obj.get("from").toString());
			
			if ( bp == null ) {
				BreakPoint a = new BreakPoint(name);
				breakPoints.add(a);
				a.addTransition(tran);
			} else {
				bp.addTransition(tran);
			}
		}
		
		return breakPoints;
	}
	
	private BreakPoint returnPoint(String name, List<BreakPoint> list) {
		for (BreakPoint i: list) {
			if ( name.equals(i.name) ) {
				return i;
			}
		}
		return null;
	}
}
