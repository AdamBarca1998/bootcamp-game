package tests;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.jupiter.api.Test;

import data.Transition;
import main.BreakPoint;

class BreakPointTest {

	private static BreakPoint bp;

    /*@Before
    public void setUp() {
    	bp = new BreakPoint("S");
    	Transition tran1 = new Transition("c", "A", "S");
    	bp.addTransition(tran1);
    }*/

    @Test
    void moveValid() {
    	bp = new BreakPoint("S");
    	Transition tran1 = new Transition("c", "A", "S");
    	bp.addTransition(tran1);
    	
    	assertEquals("A", bp.move("c"));
    }
    
    @Test
    void moveNotValid() {
    	bp = new BreakPoint("S");
    	Transition tran1 = new Transition("d", "A", "S");
    	bp.addTransition(tran1);
    	
    	assertNull(bp.move("c"));
    }
}
